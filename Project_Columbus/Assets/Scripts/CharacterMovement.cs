using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField]
    private float runSpeed;
    [SerializeField]
    private Rigidbody2D character;
    [SerializeField]
    private Camera camera;

    private Vector2 moveInput;
    private Vector2 mousePosition;
    Vector2 lookDirection;

    private bool canDash = true;
    private bool isDashing;
    private float dashingPower = 5f;
    private float dashingTime = 0.2f;
    private float dashingCooldown = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isDashing)
        {
            return;
        }
        moveInput.x = Input.GetAxisRaw("Horizontal");
        moveInput.y = Input.GetAxisRaw("Vertical");
        moveInput.Normalize();

        mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);

        if(Input.GetKeyDown(KeyCode.LeftShift) && canDash)
        {
            StartCoroutine(Dash());
        }
    }

    private void FixedUpdate()
    {
        if(isDashing) 
        {
            return;
        }

        character.MovePosition(character.position + moveInput * runSpeed * Time.fixedDeltaTime);

        Vector2 lookDirection = mousePosition - character.position;
        float angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg - 90f;
        character.rotation = angle;
    }
    
    private IEnumerator Dash()
    {
        canDash = false;
        isDashing = true;
        float originalGravity = character.gravityScale;
        character.gravityScale = 0f;
        character.velocity = new Vector2(mousePosition.x * dashingPower, mousePosition.y * dashingPower);
        yield return new WaitForSeconds(dashingTime);
        character.gravityScale = originalGravity;
        isDashing = false;
        yield return new WaitForSeconds(dashingCooldown);
        canDash = true;
    }
}
